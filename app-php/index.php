<?php

use App\ArticleRepository;
use App\ArticleService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require 'vendor/autoload.php';

$request = Request::createFromGlobals();
$articleService = new ArticleService(new ArticleRepository());

if(preg_match('/^\/article\/(\d+)$/', $request->getPathInfo(), $matches)) {
    $response = new JsonResponse($articleService->get($matches[1]));
}
elseif(preg_match('/^\/$/', $request->getPathInfo(), $matches)) {
    $response = new JsonResponse($articleService->list()->toArray());
}
else {
    $response = new JsonResponse(null, Response::HTTP_NOT_FOUND);
}

$response->send();