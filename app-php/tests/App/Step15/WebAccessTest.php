<?php

namespace App\Step15;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class WebAccessTest extends TestCase
{
    const BASE_URI = 'http://localhost:8000';
    private $httpClient;

    /**
     * @before
     */
    public function init()
    {
        $this->httpClient = new Client([
            'base_uri'    => self::BASE_URI,
            'http_errors' => false,
        ]);
    }

    /**
     * @test
     */
    public function should_return_article_list()
    {
        $response = $this->httpClient->request('GET', '/');

        $contentType = $response->getHeader('content-type')[0];
        $body        = json_decode($response->getBody()->getContents());

        $data = json_decode($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/articles-list.json'));
        self::assertThat($contentType, self::equalTo('application/json'));
        self::assertThat($body, self::equalTo($data));
    }

    /**
     * @test
     */
    public function should_return_first_article()
    {
        $response = $this->httpClient->request('GET', '/article/1');

        $contentType = $response->getHeader('content-type')[0];
        $body        = json_decode($response->getBody()->getContents());

        $data = json_decode($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/article-1.json'));
        self::assertThat($contentType, self::equalTo('application/json'));
        self::assertThat($body, self::equalTo($data));
    }

    /**
     * @test
     */
    public function should_return_second_article()
    {
        $response = $this->httpClient->request('GET', '/article/2');

        $contentType = $response->getHeader('content-type')[0];
        $body        = json_decode($response->getBody()->getContents());

        $data = json_decode($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/articles-list.json'));
        self::assertThat($contentType, self::equalTo('application/json'));
        self::assertThat($body, self::equalTo($data[0]));
    }

    /**
     * @test
     */
    public function should_return_404_on_unknown_uri()
    {
        $response = $this->httpClient->request('GET', '/wrong/url');

        self::assertThat($response->getStatusCode(), self::equalTo(Response::HTTP_NOT_FOUND));
    }

    /**
     * @test
     */
    public function should_return_404_on_unknown_article()
    {
        $response = $this->httpClient->request('GET', '/article/9999');

        self::assertThat($response->getStatusCode(), self::equalTo(Response::HTTP_NOT_FOUND));
    }

    private function getTrimmedJsonFromFile(string $path): string
    {
        return json_encode(json_decode(file_get_contents($path)), JSON_UNESCAPED_SLASHES);
    }
}