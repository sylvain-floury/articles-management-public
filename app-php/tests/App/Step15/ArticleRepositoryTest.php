<?php


namespace App\Step15;

use App\Article;
use App\ArticleRepository;
use App\UnknownArticleException;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class ArticleRepositoryTest extends TestCase
{
    /**
     * @var ArticleRepository
     */
    private $repository;

    /**
     * @before
     */
    public function init()
    {
        $articles = new ArrayCollection();
        $articles->add(new Article(
            2,
            "Second article",
            "Text of second article",
            "2019-10-29",
            "Jane Doe",
            "https://fake.info/article/2019-10-29-second-article"
        ));
        $articles->add(new Article(
            1,
            "First article",
            "Text of first article",
            "2019-10-30",
            "Jane Doe",
            "https://fake.info/article/2019-10-30-first-article"
        ));
        $this->repository = new ArticleRepository($articles);
    }

    /**
     * @test
     */
    public function should_find_all_articles()
    {
        $articleList = $this->repository->findAll();

        self::assertThat($articleList, self::countOf(2));
        self::assertThat($articleList, self::containsOnlyInstancesOf(Article::class));
    }

    /**
     * @test
     */
    public function should_get_a_article_from_its_id()
    {
        $article = $this->repository->get(1);

        self::assertThat($article, self::isInstanceOf(Article::class));
        self::assertThat($article->getId(), self::equalTo(1));
        self::assertThat($article->getTitle(), self::equalTo('First article'));
        self::assertThat($article->getText(), self::equalTo('Text of first article'));
        self::assertThat($article->getDate(), self::equalTo('2019-10-30'));
        self::assertThat($article->getAuthor(), self::equalTo('Jane Doe'));
        self::assertThat($article->getUrl(), self::equalTo('https://fake.info/article/2019-10-30-first-article'));

    }

    /**
     * @test
     */
    public function should_throw_exception_for_unknown_article_id()
    {
        $this->expectException(UnknownArticleException::class);
        $this->repository->get(100);
    }


    /**
     * @test
     */
    public function should_find_1_as_next_id()
    {
        $articleRepository = new ArticleRepository(new ArrayCollection());

        $nextId = $articleRepository->nextId();

        self::assertThat($nextId, self::equalTo(1));
    }

    /**
     * @test
     */
    public function should_find_3_as_next_id()
    {
        $nextId = $this->repository->nextId();

        self::assertThat($nextId, self::equalTo(3));
    }

    /**
     * @test
     */
    public function should_add_an_article()
    {
        $article = new Article(null, "Third article", "Text of the third article", "2019-11-02", "Edsger Dijkstra",
            "https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra");

        $article = $this->repository->add($article);

        self::assertThat($this->repository->findAll(), self::countOf(3));
        self::assertThat($article, self::equalTo($this->repository->get(3)));
    }

    /**
     * @test
     */
    public function should_update_article()
    {
        $article = new Article(2, "Changed article", "New content of article", "2019-11-04", "John Doe",
            "http://www.example.net");

        $this->repository->update($article);

        $article = $this->repository->get(2);
        self::assertThat($article->getId(), self::equalTo(2));
        self::assertThat($article->getTitle(), self::equalTo('Changed article'));
        self::assertThat($article->getText(), self::equalTo('New content of article'));
        self::assertThat($article->getDate(), self::equalTo('2019-11-04'));
        self::assertThat($article->getAuthor(), self::equalTo('John Doe'));
        self::assertThat($article->getUrl(), self::equalTo('http://www.example.net'));
    }

    /**
     * @test
     */
    public function should_throw_exception_on_update_inexistant_article()
    {
        $article = new Article(40, "Changed article", "New content of first article", "2019-11-04", "John Doe",
            "http://www.example.net");

        $this->expectException(UnknownArticleException::class);
        $this->repository->update($article);
    }


    /**
     * @test
     */
    public function should_delete_article()
    {
        $articleToDelete = $this->repository->get(1);

        $this->repository->delete($articleToDelete);

        self::assertThat($this->repository->findAll(), self::countOf(1));
        $this->expectException(UnknownArticleException::class);
        $this->repository->get(1);
    }
}