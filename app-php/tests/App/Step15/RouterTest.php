<?php


namespace App\Step15;


use App\ArticleController;
use App\Router;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RouterTest extends TestCase
{
    /**
     * @test
     */
    public function should_call_list_method_from_ArticleController()
    {
        $controller = $this->createMock(ArticleController::class);
        $router = new Router();
        $router->add('/^\/$/', $controller, 'list');
        $controller->expects(self::once())
            ->method('list');

        $request = $this->createMock(Request::class);
        $request->method('getPathInfo')
            ->willReturn('/');

        $response = $router->execute($request);

        self::assertThat($response, self::isInstanceOf(Response::class));
    }

    /**
     * @test
     */
    public function should_call_get_method_from_ArticleController()
    {
        $controller = $this->createMock(ArticleController::class);
        $router = new Router();
        $router->add('/^\/article\/(\d+)$/', $controller,'get');
        $controller->expects(self::once())
            ->method('get');

        $request = $this->createMock(Request::class);
        $request->method('getPathInfo')
            ->willReturn('/article/1');

        $response = $router->execute($request);

        self::assertThat($response, self::isInstanceOf(Response::class));
    }

    /**
     * @test
     */
    public function should_return_HTTP_NOT_FOUND_Response_when_no_route_match()
    {
        $controller = $this->createMock(ArticleController::class);
        $router = new Router();

        $request = $this->createMock(Request::class);

        $response = $router->execute($request);

        self::assertThat($response, self::isInstanceOf(Response::class));
        self::assertThat($response->getStatusCode(), self::equalTo(Response::HTTP_NOT_FOUND));
    }
}