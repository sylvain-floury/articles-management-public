<?php

namespace App\Step03;

use App\ArticleRepository;
use App\ArticleService;
use App\UnknownArticleException;
use PHPUnit\Framework\TestCase;

class ArticleServiceTest extends TestCase
{
    private $articleRepository;

    /**
     * @before
     */
    public function init()
    {
        $this->articleRepository = new ArticleRepository();
    }

    /**
     * @test
     */
    public function should_list_articles_as_json()
    {
        $service = new ArticleService($this->articleRepository);

        $articleList = $service->list();

        self::assertThat($articleList,
            self::equalTo($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/articles-list.json')));
    }

    /**
     * @test
     */
    public function should_find_article_by_id()
    {
        $service = new ArticleService($this->articleRepository);

        $article = $service->get(1);

        self::assertThat($article,
            self::equalTo($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/article-1.json')));
    }

    /**
     * @test
     */
    public function should_throw_exception_for_unknown_article_id()
    {
        $service = new ArticleService($this->articleRepository);

        $this->expectException(UnknownArticleException::class);
        $service->get(100);
    }


    private function getTrimmedJsonFromFile(string $path): string
    {
        return json_encode(json_decode(file_get_contents($path)), JSON_UNESCAPED_SLASHES);
    }
}

