<?php

namespace App\Step13;

use App\ArticleRepository;
use App\CsvEntityManager;
use App\JsonEntityManager;
use PHPUnit\Framework\TestCase;

class CsvEntityManagerTest extends TestCase
{
    const JSON_DATA_SOURCE = __DIR__ . '/../../../resources/entity-manager.json';
    const CSV_DATA_SOURCE = __DIR__ . '/../../../data/articles-list.csv';

    /**
     * @beforeClass
     */
    public static function init()
    {
        if (file_exists(self::CSV_DATA_SOURCE)) {
            unlink(self::CSV_DATA_SOURCE);
        }
    }

    /**
     * @test
     */
    public function should_convert_json_storage_to_csv()
    {
        $jsonEntityManager = new JsonEntityManager(self::JSON_DATA_SOURCE);
        $csvEntityManager  = new CsvEntityManager(self::CSV_DATA_SOURCE);

        $articleRepositoryFromJson = new ArticleRepository($jsonEntityManager->load());
        $csvEntityManager->persist($articleRepositoryFromJson);

        self::assertThat(file_get_contents(self::CSV_DATA_SOURCE),
        self::equalTo(file_get_contents(__DIR__ . '/../../../resources/entity-manager.csv')));
    }
}
