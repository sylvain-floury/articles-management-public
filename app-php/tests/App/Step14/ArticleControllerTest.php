<?php

namespace App\Step14;

use App\ArticleController;
use App\ArticleService;
use App\JsonEntityManager;
use App\UnknownArticleException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ArticleControllerTest extends TestCase
{
    /**
     * @test
     */
    public function should_list_all_articles()
    {
        $articleService = $this->createMock(ArticleService::class);
        $articleService->method('list')->willReturn($this->getListArticle());
        $controller = new ArticleController($articleService);

        $response = $controller->list();

        self::assertThat($response, self::isInstanceOf(JsonResponse::class));
        self::assertThat($response->getContent(),
            self::equalTo($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/articles-list.json')));
    }

    /**
     * @test
     */
    public function should_get_article_from_id()
    {
        $articleService = $this->createMock(ArticleService::class);
        $articleService->method('get')->with(self::isType('int'))->willReturn($this->getArticle());
        $controller = new ArticleController($articleService);

        $response = $controller->get(1);

        self::assertThat($response, self::isInstanceOf(JsonResponse::class));
        self::assertThat($response->getContent(),
            self::equalTo($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/article-1.json')));
    }

    /**
     * @test
     */
    public function should_return_404_on_unknown_id()
    {
        $articleService = $this->createMock(ArticleService::class);
        $articleService->method('get')->with(self::isType('int'))->willThrowException(new UnknownArticleException('Unknown article'));
        $controller = new ArticleController($articleService);

        $response = $controller->get(100);

        self::assertThat($response->getStatusCode(), self::equalTo(Response::HTTP_NOT_FOUND));
    }

    private function getListArticle()
    {
        $entityManager = new JsonEntityManager(__DIR__ . '/../../../resources/articles-list.json');

        return $entityManager->load();
    }

    private function getTrimmedJsonFromFile(string $path): string
    {
        return json_encode(json_decode(file_get_contents($path)));
    }

    private function getArticle()
    {
        return $this->getListArticle()->last();
    }
}