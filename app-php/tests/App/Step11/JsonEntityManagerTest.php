<?php

namespace App\Step11;

use App\Article;
use App\ArticleRepository;
use App\JsonEntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class JsonEntityManagerTest extends TestCase
{
    const DATA_SOURCE = __DIR__ . '/../../../data/articles-list.json';

    /**
     * @beforeClass
     */
    public static function init()
    {
        if (file_exists(self::DATA_SOURCE)) {
            unlink(self::DATA_SOURCE);
        }
    }

    /**
     * @test
     */
    public function should_load_from_json_file()
    {
        $entityManager             = new JsonEntityManager(__DIR__ . '/../../../resources/articles-list.json');
        $articleRepositoryFromJson = new ArticleRepository($entityManager->load());

        $article = $articleRepositoryFromJson->get(1);

        self::assertThat($articleRepositoryFromJson->findAll(), self::countOf(2));
        self::assertThat($article, self::isInstanceOf(Article::class));
        self::assertThat($article->getId(), self::equalTo(1));
        self::assertThat($article->getTitle(), self::equalTo('First article'));
        self::assertThat($article->getText(), self::equalTo('Text of first article'));
        self::assertThat($article->getDate(), self::equalTo('2019-10-30'));
        self::assertThat($article->getAuthor(), self::equalTo('Jane Doe'));
        self::assertThat($article->getUrl(), self::equalTo('https://fake.info/article/2019-10-30-first-article'));
    }

    /**
     * @test
     */
    public function should_persist_to_json_file()
    {
        $entityManager     = new JsonEntityManager(self::DATA_SOURCE);
        $articleRepository = new ArticleRepository(new ArrayCollection());
        $article           = new Article(null, "Third article", "Text of the third article", "2019-11-02",
            "Edsger Dijkstra", "https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra");
        $articleRepository->add($article);

        $entityManager->persist($articleRepository);
        $dataSourceContent = file_get_contents(self::DATA_SOURCE);

        self::assertThat(self::DATA_SOURCE, self::fileExists());
        self::assertThat($dataSourceContent,
            self::equalTo(file_get_contents(__DIR__ . '/../../../resources/entity-manager.json')));
    }

    private function getTrimmedJsonFromFile(string $path): string
    {
        return json_encode(json_decode(file_get_contents($path)), JSON_UNESCAPED_SLASHES);
    }
}
