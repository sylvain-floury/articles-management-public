<?php

namespace App\Step12;

use App\Article;
use App\ArticleRepository;
use App\CsvEntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class CsvEntityManagerTest extends TestCase
{
    const DATA_SOURCE = __DIR__ . '/../../../data/articles-list.csv';

    /**
     * @beforeClass
     */
    public static function init()
    {
        if (file_exists(self::DATA_SOURCE)) {
            unlink(self::DATA_SOURCE);
        }
    }

    /**
     * @test
     */
    public function should_load_from_csv_file()
    {
        $entityManager            = new CsvEntityManager(__DIR__ . '/../../../resources/entity-manager.csv');
        $articleRepositoryFromCsv = new ArticleRepository($entityManager->load());

        $article = $articleRepositoryFromCsv->get(1);

        self::assertThat($articleRepositoryFromCsv->findAll(), self::countOf(1));
        self::assertThat($article, self::isInstanceOf(Article::class));
    }

    /**
     * @test
     */
    public function should_persist_to_csv_file()
    {
        $entityManager     = new CsvEntityManager(self::DATA_SOURCE);
        $articleRepository = new ArticleRepository(new ArrayCollection());
        $article           = new Article(null, "Third article", "Text of the third article", "2019-11-02",
            "Edsger Dijkstra", "https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra");
        $articleRepository->add($article);

        $entityManager->persist($articleRepository);
        $dataSourceContent = file_get_contents(self::DATA_SOURCE);

        self::assertThat(self::DATA_SOURCE, self::fileExists());
        self::assertThat($dataSourceContent,
            self::equalTo(file_get_contents(__DIR__ . '/../../../resources/entity-manager.csv')));
    }
}
