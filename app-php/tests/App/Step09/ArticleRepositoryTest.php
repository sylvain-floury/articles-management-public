<?php


namespace App\Step09;

use App\Article;
use App\ArticleRepository;
use App\UnknownArticleException;
use PHPUnit\Framework\TestCase;

class ArticleRepositoryTest extends TestCase
{
    /**
     * @var ArticleRepository
     */
    private $repository;

    /**
     * @before
     */
    public function init()
    {
        $this->repository = new ArticleRepository();
    }

    /**
     * @test
     */
    public function should_find_all_articles()
    {
        $articleList = $this->repository->findAll();

        self::assertThat($articleList, self::countOf(2));
        self::assertThat($articleList, self::containsOnlyInstancesOf(Article::class));
    }

    /**
     * @test
     */
    public function should_get_a_article_from_its_id()
    {
        $article = $this->repository->get(1);

        self::assertThat($article, self::isInstanceOf(Article::class));
        self::assertThat($article->getId(), self::equalTo(1));
        self::assertThat($article->getTitle(), self::equalTo('First article'));
        self::assertThat($article->getText(), self::equalTo('Text of first article'));
        self::assertThat($article->getDate(), self::equalTo('2019-10-30'));
        self::assertThat($article->getAuthor(), self::equalTo('Jane Doe'));
        self::assertThat($article->getUrl(), self::equalTo('https://fake.info/article/2019-10-30-first-article'));

    }

    /**
     * @test
     */
    public function should_throw_exception_for_unknown_article_id()
    {
        $this->expectException(UnknownArticleException::class);
        $this->repository->get(100);
    }


    /**
     * @test
     */
    public function should_find_1_as_next_id()
    {
        $articleRepository = new ArticleRepository(__DIR__ . '/../../../resources/empty-articles-list.json');

        $nextId = $articleRepository->nextId();

        self::assertThat($nextId, self::equalTo(1));
    }

    /**
     * @test
     */
    public function should_find_3_as_next_id()
    {
        $articleRepository = new ArticleRepository();

        $nextId = $articleRepository->nextId();

        self::assertThat($nextId, self::equalTo(3));
    }

    /**
     * @test
     */
    public function should_add_an_article()
    {
        $article = new Article(null, "Third article", "Text of the third article", "2019-11-02", "Edsger Dijkstra",
            "https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra");

        $articleRepository = new ArticleRepository();
        $article = $articleRepository->add($article);

        self::assertThat($articleRepository->findAll(), self::countOf(3));
        self::assertThat($article, self::equalTo($articleRepository->get(3)));
    }

    /**
     * @test
     */
    public function should_update_article()
    {
        $articleRepository = new ArticleRepository();
        $article = new Article(1, "Changed article", "New content of first article", "2019-11-04", "John Doe",
            "http://www.example.net");

        $articleRepository->update($article);

        $article = $articleRepository->get(1);
        self::assertThat($article->getId(), self::equalTo(1));
        self::assertThat($article->getTitle(), self::equalTo('Changed article'));
        self::assertThat($article->getText(), self::equalTo('New content of first article'));
        self::assertThat($article->getDate(), self::equalTo('2019-11-04'));
        self::assertThat($article->getAuthor(), self::equalTo('John Doe'));
        self::assertThat($article->getUrl(), self::equalTo('http://www.example.net'));
    }

    /**
     * @test
     */
    public function should_throw_exception_on_update_inexistant_article()
    {
        $articleRepository = new ArticleRepository();
        $article = new Article(40, "Changed article", "New content of first article", "2019-11-04", "John Doe",
            "http://www.example.net");

        $this->expectException(UnknownArticleException::class);
        $articleRepository->update($article);
    }
}