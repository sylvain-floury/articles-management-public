<?php


namespace App;


class Article implements \JsonSerializable
{
    private int $id;
    private string $title;
    private string $text;
    private string $date;
    private string $author;
    private string $url;

    public function __construct(int $id, string $title, string $text, string $date, string $author, string $url)
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
        $this->date = $date;
        $this->author = $author;
        $this->url = $url;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'date' => $this->date,
            'author' => $this->author,
            'url' => $this->url
        ];
    }
}