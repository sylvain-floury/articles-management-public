<?php


namespace App;


use Doctrine\Common\Collections\ArrayCollection;

class ArticleRepository
{

    private $articles;

    public function __construct()
    {
        $this->articles = new ArrayCollection(json_decode(file_get_contents(__DIR__ . '/../../resources/articles-list.json')));

        $this->articles = $this->articles->map(function ($article) {
            return new Article(
                $article->id,
                $article->title,
                $article->text,
                $article->date,
                $article->author,
                $article->url
            );
        });
    }

    public function findAll()
    {
        return $this->articles;
    }

    public function get(int $id)
    {
        $filterResult = $this->articles->filter(function ($article) use ($id) {
            return $article->getId() == $id;
        });

        if($filterResult->isEmpty()) {
            throw new UnknownArticleException('Unknown article');
        }

        return $filterResult->first();
    }
}