<?php


namespace App;


class ArticleService
{
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }


    public function list()
    {
        return $this->articleRepository->findAll();
    }

    public function get(int $id)
    {
        return $this->articleRepository->get($id);
    }
}
